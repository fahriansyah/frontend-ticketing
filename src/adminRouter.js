import Dashboard from './views/admin/Dashboard.vue'
import ListBooking from './views/admin/booking/ListBooking.vue'
import DetailBooking from './views/admin/booking/DetailBooking.vue'

import ListSchedule from './views/admin/schedule/ListSchedule.vue'
import CreateSchedule from './views/admin/schedule/CreateSchedule.vue'
import UpdateSchedule from './views/admin/schedule/UpdateSchedule.vue'

import ListTransportation from './views/admin/transportation/ListTransportation.vue'
import CreateTransportation from './views/admin/transportation/CreateTransportation.vue'
import UpdateTransportation from './views/admin/transportation/UpdateTransportation.vue'

const AdminRouter = [
	{
		path: 'dashboard',
		name: 'admin.dashboard',
		component: Dashboard
	},
	{
		path: 'booking',
		name: 'admin.booking',
		component: ListBooking
	},
	{
		path: 'booking/:id',
		name: 'admin.booking.detail',
		component: DetailBooking
	},
	{
		path: 'schedule',
		name: 'admin.schedule',
		component: ListSchedule
	},
	{
		path: 'schedule/create',
		name: 'admin.schedule.create',
		component: CreateSchedule
	},
	{
		path: 'schedule/update/:id',
		name: 'admin.schedule.update',
		component: UpdateSchedule
	},
	{
		path: 'transportation',
		name: 'admin.transportation',
		component: ListTransportation
	},
	{
		path: 'transportation/create',
		name: 'admin.transportation.create',
		component: CreateTransportation
	},
	{
		path: 'transportation/update/:id',
		name: 'admin.transportation.update',
		component: UpdateTransportation
	}
];

export default AdminRouter