import axios from 'axios'

const instance = axios.create({
	baseURL: "http://localhost:9000"
})

if (localStorage.getItem("api_key")) {
	instance.defaults.headers['Authorization'] = "Bearer " + localStorage.getItem("api_key")
}

export default instance