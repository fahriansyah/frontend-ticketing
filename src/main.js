import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import VueElementLoading from 'vue-element-loading'
import VueQrcode from '@chenfengyuan/vue-qrcode'

Vue.config.productionTip = false
Vue.component('VueElementLoading', VueElementLoading)
Vue.component('v-select', VueSelect.VueSelect)
Vue.component(VueQrcode.name, VueQrcode)

const options = {
  toast: {
  	position: SnotifyPosition.rightBottom
  }
}

Vue.use(Snotify, options)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
