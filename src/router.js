import Vue from 'vue'
import Router from 'vue-router'
import http from './http'
import Home from './views/Home.vue'
import Login from './views/auth/Login.vue'
import Register from './views/auth/Register.vue'

import User from './views/user/User.vue'
import UserRouter from './userRouter'

import Admin from './views/admin/Admin.vue'
import AdminRouter from './adminRouter'

Vue.use(Router)

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/login',
			name: 'login',
			component: Login
		},
		{
			path: '/register',
			name: 'register',
			component: Register
		},
		{
			path: '/u',
			name: 'user',
			component: User,
			children : UserRouter,
			beforeEnter: (to, from, next) => {
				if (!localStorage.getItem("api_key")) {
					return next("/login")
				}else{
					http.post("auth/check", {
						apiKey : localStorage.getItem("api_key")
					})
					.then((res) => {
						return next()
					})
					.catch((err) => {
						if (err.response) {
							if (err.response.status == 500) {
								console.log("something went wrong from backend")
								this.$snotify.error("something went wrong from backend", "Error",{
									timeout: 4000,
									showProgressBar: true,
									closeOnClick: false,
									pauseOnHover: true
								})
							}else if (err.response.status == 403) {
								return next("/login")
							}
						}
					})
				}
			}
		},
		{
			path: '/a',
			name: 'admin',
			component: Admin,
			children: AdminRouter
		}
	]
})
