import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		isAuthenticated: false,
		user: {
			avatar: '',
			firstname: '',
			lastname: '',
			email: '',
			phone: '',
			birthdate: '',
			address: '',
			api_key: '',
			role: '',
		}
	},
	mutations: {

	},
	actions: {

	}
})
