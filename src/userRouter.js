import Dashboard from './views/user/Dashboard.vue'
import Booking from './views/user/booking/Booking.vue'
import DetailBooking from './views/user/booking/DetailBooking.vue'
import DetailTicket from './views/user/ticket/DetailTicket.vue'
import ExportPdf from './views/user/ticket/ExportPdf.vue'


const UserRouter = [
	{
		path: 'dashboard',
		name: 'user.dashboard',
		component: Dashboard
	},
	{
		path: 'booking',
		name: 'user.booking',
		component: Booking
	},
	{
		path: 'booking/:id',
		name: 'user.booking.detail',
		component: DetailBooking
	},
	{
		path: 'ticket/:id',
		name: 'user.ticket.detail',
		component: DetailTicket
	},
	{
		path: 'ticket/export/:id',
		name: 'user.ticket.export',
		component: ExportPdf
	}
]

export default UserRouter